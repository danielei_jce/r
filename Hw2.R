train_set <- read.csv('train.csv')


train_set_fixed <- subset(train_set, select = c("Survived","Pclass", "Sex", "Age", "SibSp", "Parch", "Embarked"))


install.packages('mice')
library('mice')

train_meanAge <- mice(train_set_fixed,m=1,maxit = 1,meth ='mean',seed = 500)

train_set_fixed <- complete(train_meanAge,1)

conv_age <- function(x){
  if(x<18){
    x<- 1
  }
  else if (x>60){
    x<- 3
  }
  else{
    x<- 2
  }
  
  x<- factor(x,levels = c(1,2,3) , labels = c('child','adult','old'))
}

train_set_fixed$Age <- sapply(train_set_fixed$Age, conv_age)
str(train_set_fixed)

conv_ps <- function(x){
  if(x>0){
    x<- 1
  }
  else{
    x<- 2
  }
  
  x<- factor(x,levels = c(1,2) , labels = c('Yes','No'))
}

train_set_fixed$SibSp <- sapply(train_set_fixed$SibSp,conv_ps)
train_set_fixed$Parch <- sapply(train_set_fixed$Parch,conv_ps)

conv_pclass <- function(x){
  
  x<- factor(x,levels = c(1,2,3) , labels = c('1st','2nd','3rd'))
}

train_set_fixed$Pclass <- sapply(train_set_fixed$Pclass,conv_pclass)

conv_yesno <- function(x){
  
  x<- factor(x,levels = c(0,1) , labels = c('No','Yes'))
}

train_set_fixed$Survived <- sapply(train_set_fixed$Survived,conv_yesno)

split<-runif(nrow(train_set))>0.3

train_raw <- train_set_fixed[split,]
test_raw <- train_set_fixed[!split,]

install.packages('e1071')
library(e1071)

model <- naiveBayes(train_raw[,-1],train_raw$Survived)

prediction <- predict(model,test_raw[,-1])
prediction

install.packages('SDMTools')
library(SDMTools)

conv_10 <- function(x){
  x<-ifelse(x == 'Yes',1,0)
}
pred01 <- sapply(prediction,conv_10)
actual01<- sapply(test_raw$Survived,conv_10)


confusion.matrix(actual01,pred01)

predictionTrain <- predict(model,train_raw[,-1])

predTrain01 <- sapply(predictionTrain,conv_10)
actualTrain01<- sapply(train_raw$Survived,conv_10)


confusion.matrix(actualTrain01,predTrain01)

